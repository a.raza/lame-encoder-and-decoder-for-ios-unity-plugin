﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    public AudioSource audioSource;
    public string mp3FileName, wavFileName;

    public void startRecording()
    {
        audioSource.clip = Microphone.Start("Built-in Microphone", true, 10, 48000);
        audioSource.Play();
    }

    public void stopRecording()
    {
        Microphone.End("Built-in Microphone");

        audioSource.Play();

        float[] samples = new float[audioSource.clip.samples * audioSource.clip.channels];
        audioSource.clip.GetData(samples, 0);

        for (int i = 0; i < samples.Length; ++i)
        {
            samples[i] = samples[i] * 0.5f;
        }

        audioSource.clip.SetData(samples, 0);

        iOSSupport.convert2MP3(samples, samples.Length, 48000, mp3FileName + ".mp3");
    }

    public void convertMP32WAV()
    {
        iOSSupport.convertMP32WAV(mp3FileName + ".mp3", wavFileName + ".wav");
    }
}
