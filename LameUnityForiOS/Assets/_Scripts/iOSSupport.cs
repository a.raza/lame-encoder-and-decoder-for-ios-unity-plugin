﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class iOSSupport : MonoBehaviour 
{
    [DllImport("__Internal")]
    public static extern bool convert2MP3(float[] recordedData, int sizeOfData, int sampleRate, string fileName);

    [DllImport("__Internal")]
    public static extern bool convertMP32WAV(string mp3FileName, string WAVfileName);

}
