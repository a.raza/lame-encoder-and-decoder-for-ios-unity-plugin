//
//  AudioFile.h
//
//  Created by Ammad Raza on 10/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <lame/lame.h>

NS_ASSUME_NONNULL_BEGIN

const int PCM_SIZE = 4096;
const int MP3_SIZE = 4096;

@interface AudioFile : NSObject

-(BOOL) encode: (float*) recordedData
     datasize : (int) size
    sampleRate: (int) nSampleRate
       docName: (char*) fileName;

-(BOOL) decode: (char*) getAudioPath
    outputFile: (char*) outputFilePath;

-(void) WriteWaveHeader:(FILE*)file pcm_bytes:(int)pcmbytes _freq:(int)freq _channels:(int)channels _bits:(int)bits;

-(void) write_16_bits_low_high:(FILE*)file value:(int)val;

-(void) write_32_bits_low_high:(FILE*)file value:(int)val;

@end

NS_ASSUME_NONNULL_END
