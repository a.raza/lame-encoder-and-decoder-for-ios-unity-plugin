
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <cstdint>
#include <list>

#import "AudioFile.h"

using namespace std;

extern "C"
{
    BOOL convert2MP3(float* recordedData, int size, int sampleRate, char* fileName)
    {
        AudioFile *audioEn = [[AudioFile alloc] init];
        
        return [audioEn encode:recordedData datasize:size sampleRate:sampleRate docName:fileName];
    }
    
    BOOL convertMP32WAV(char* mp3FileName, char* wavFileName)
    {
        AudioFile *audioEn = [[AudioFile alloc] init];
        
        return [audioEn decode:mp3FileName outputFile:wavFileName];
    }
}
