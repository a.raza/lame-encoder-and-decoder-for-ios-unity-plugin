//
//  AudioFile.mm
//
//  Created by Ammad Raza on 10/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//

#import "AudioFile.h"
#import <AVFoundation/AVFoundation.h>

@implementation AudioFile

short int pcm_buffer_I_L[PCM_SIZE];
short int pcm_buffer_I_R[PCM_SIZE];
unsigned char mp3buf[MP3_SIZE];

-(BOOL) encode: (float*) recordedData // Float Array
     datasize : (int) size            // Size of Float Array
    sampleRate: (int) nSampleRate     // Sample rate
       docName: (char*) fileName      // File Name
{
    NSString *file = [NSString stringWithCString:fileName encoding:NSASCIIStringEncoding];
    
    NSString *tmpUrl = [NSString stringWithFormat:@"%@/%@", NSTemporaryDirectory(), file];
    
    NSLog(@"Mp3 File Path: %@", tmpUrl);
    
    float write;
    
    //Converting into mp3, so we need the mp3 file ready!
    FILE *mp3 = fopen((char*)[tmpUrl UTF8String], "wb");
    
    lame_t lame = lame_init();
    lame_set_in_samplerate(lame, nSampleRate);
    lame_set_out_samplerate(lame, nSampleRate);
    lame_set_VBR(lame, vbr_max_indicator);
    lame_set_mode(lame, MONO); //if this is enabled the channels will be set to 1 by default
    //    lame_set_num_channels(lame, 1);
    lame_set_quality(lame, 0); //0 is the best .. 9 is the worst
    lame_init_params(lame);
    
    NSLog(@"Sample Rate: %d " , lame_get_in_samplerate(lame));
    
    /*
     lame_encode_buffer_interleaved_ieee_float
     1st pram = lame config
     2nd pram = float
     3rd pram = frame
     4th pram = mp3buf //mp3buf is an array of unsigned char with size of 4096
     5th pram = size of Mp3 which is 4096
     */
    
    int i;
    for (i = 0; i < size; i++)
    {
        write = lame_encode_buffer_interleaved_ieee_float(lame, &recordedData[i], 1, mp3buf, MP3_SIZE);
        
        fwrite(mp3buf, write, 1, mp3);
    }
    
    lame_close(lame);
    
    fclose(mp3);
    
    return YES;
}

-(BOOL) decode:(char *)mp3FileName
    outputFile:(char *)outputFilePath
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    NSString *file = [NSString stringWithCString:mp3FileName encoding:NSASCIIStringEncoding];
    
    NSString *tmpInput = [NSString stringWithFormat:@"%@/%@", NSTemporaryDirectory(), file];
    
    if([filemgr fileExistsAtPath:tmpInput])
    {
        NSLog(@"Mp3 File Found");
    }
    else
    {
        NSLog(@"Mp3 File NOT Found, (Cannot Proceed With Decoding Process)");
        return false;
    }
    
    NSLog(@"MP3 File At Path: %@", tmpInput);
    
    NSString *outputFile = [NSString stringWithCString:outputFilePath encoding:NSASCIIStringEncoding];
    
    NSString *tmpOutput = [NSString stringWithFormat:@"%@/%@", NSTemporaryDirectory(), outputFile];
    
    NSLog(@"WAV File At Path: %@", tmpOutput);
    
    int read, i, samples;
    long wavsize = 0; // use to count the number of mp3 byte read, this is used to write the length of the wave file
    
    FILE *mp3 = fopen((char*)[tmpInput UTF8String], "rb");
    
    FILE* pcm = fopen((char*)[tmpOutput UTF8String], "wb");
    
    lame_t lame = lame_init();
    lame_set_decode_only(lame, 1);
    
    hip_t hip = hip_decode_init();
    
    mp3data_struct mp3data;
    memset(&mp3data, 0, sizeof(mp3data));
    
    int nChannels = -1;
    int nSampleRate = -1;
    int mp3_len;
    
    while((read = fread(mp3buf, sizeof(char), 4096, mp3)) > 0)
    {
        mp3_len = read;
        do
        {
            samples = hip_decode1_headers(hip, mp3buf, mp3_len, pcm_buffer_I_L, pcm_buffer_I_L, &mp3data);
            
            wavsize += samples;
            
            if(mp3data.header_parsed == 1)//header is gotten
            {
                if(nChannels < 0)//reading for the first time
                {
                    //Write the header
                    [self WriteWaveHeader:pcm pcm_bytes:0x7FFFFFFF _freq:mp3data.samplerate _channels:mp3data.stereo _bits:16];
                }
                
                nChannels = mp3data.stereo;
                nSampleRate = mp3data.samplerate;
            }
            
            if(samples > 0)
            {
                for(i = 0 ; i < samples; i++)
                {
                    fwrite((char*)&pcm_buffer_I_L[i], sizeof(char), sizeof(pcm_buffer_I_L[i]), pcm);
                    if(nChannels == 2)
                    {
                        fwrite((char*)&pcm_buffer_I_R[i], sizeof(char), sizeof(pcm_buffer_I_R[i]), pcm);
                    }
                }
            }
            mp3_len = 0;
            
        }while(samples>0);
    }
    
    i = (16 / 8) * mp3data.stereo;
    if (wavsize <= 0)
    {
        wavsize = 0;
    }
    else if (wavsize > 0xFFFFFFD0 / i)
    {
        wavsize = 0xFFFFFFD0;
    }
    else
    {
        wavsize *= i;
    }
    
    if(!fseek(pcm, 0l, SEEK_SET))//seek back and adjust length
        [self WriteWaveHeader:pcm pcm_bytes:(int) wavsize _freq:mp3data.samplerate _channels:mp3data.stereo _bits:16];
    
    hip_decode_exit(hip);
    lame_close(lame);
    fclose(mp3);
    fclose(pcm);
    
    return YES;
}

-(void) WriteWaveHeader:(FILE*)file pcm_bytes:(int)pcmbytes _freq:(int)freq _channels:(int)channels _bits:(int)bits
{
    int     bytes = (bits + 7) / 8;
    
    /* quick and dirty, but documented */
    fwrite("RIFF", 1, 4, file); /* label */
    
    [self write_32_bits_low_high:file value:pcmbytes + 44 - 8];
    
    fwrite("WAVEfmt ", 2, 4, file); /* 2 labels */
    
    [self write_32_bits_low_high:file value:2 + 2 + 4 + 4 + 2 + 2];
    
    [self write_16_bits_low_high:file value:1];
    
    [self write_16_bits_low_high:file value:channels];
    
    [self write_32_bits_low_high:file value:freq];
    
    [self write_32_bits_low_high:file value:freq * channels * bytes];
    
    [self write_16_bits_low_high:file value:channels * bytes];
    
    [self write_16_bits_low_high:file value:bits];
    
    fwrite("data", 1, 4, file);
    
    [self write_32_bits_low_high:file value:pcmbytes];
}

-(void) write_16_bits_low_high:(FILE*)file value:(int)val
{
    unsigned char bytes[2];
    bytes[0] = (val & 0xff);
    bytes[1] = ((val >> 8) & 0xff);
    fwrite(bytes, 1, 2, file);
}

-(void) write_32_bits_low_high:(FILE*)file value:(int)val
{
    unsigned char bytes[4];
    bytes[0] = (val & 0xff);
    bytes[1] = ((val >> 8) & 0xff);
    bytes[2] = ((val >> 16) & 0xff);
    bytes[3] = ((val >> 24) & 0xff);
    fwrite(bytes, 1, 4, file);
}

@end
